<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;

class PostsController extends Controller
{
    function getPost()
    {
        $post = Post::get(); 

        return response()->json($post, 200);
    }

    function addPost(Request $request)
    {
       //DB transaction
       DB::beginTransaction(); 
       try
           {
              $this->validate($request,[
                   'title' => 'required', 
                   'content' => 'required' 
               ]);

               //save ke database
               $title = $request->input('title'); 
               $content = $request->input('content');

               // save ke database menggunakan metode eloquen
               $newPost = new Post;
               $newPost->title = $title;
               $newPost->content = $content;
               $newPost->save();

               $post = Post::get();

               DB::commit(); 
               return response()->json($post, 200);
           }
       catch (\Exception $e)
           {
               DB::rollback(); 
               return response()->json(["message" => $e->getMessage()], 500); 
           }
    }
    function deletePost(Request $request)
    {
        DB::beginTransaction();
        try
            {

                $id = (integer)$request->input('id');
                $post = Post::find($id);

                if(empty($post))
                    {
                        return response()->json(["message" => "User not found"], 404);
                    }

                $post->delete();

                $post = Post::get();

            
                DB::commit();
                return response()->json(["message" => "Success !!"], 200);
            }
        catch (\Exception $e)
            {
                DB::rollback();
                return response()->json(["message" => $e->getMessage()], 500);
            }        
        }

        public function updatePost($id)
        {
            DB::beginTransaction();
            try
                {
    
                    $id = (integer)$request->input('id');
                    $post = Post::find($id);
    
                    $post->update($newPost);
    
                    $post = Post::get();
    
                
                    DB::commit();
                    return response()->json(["message" => "Success !!"], 200);
                }
            catch (\Exception $e)
                {
                    DB::rollback();
                    return response()->json(["message" => $e->getMessage()], 500);
                }
        }
}
